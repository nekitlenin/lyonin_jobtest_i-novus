package ru.pcs.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.pcs.web.services.NumbersService;
import ru.pcs.web.services.NumbersServiceImpl;

/**
 * Project: carNumberGenerator
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 25.12.2021 03:39 |
 * Created with IntelliJ IDEA
 */
@Controller
public class NumbersController {

    private final NumbersService numbersService = new NumbersServiceImpl();

    @GetMapping("number/random")
    public String getRandomNumber(Model model) {
        String randomNumber = numbersService.random();

        model.addAttribute("randomNumber", randomNumber);
        return "randomNumber";
    }

    @GetMapping("number/next")
    public String getNextNumber(Model model) {
        String nextNumber = numbersService.next();

        model.addAttribute("nextNumber", nextNumber);
        return "nextNumber";
    }
}
