package ru.pcs.web.services;

/**
 * Project: carNumberGenerator
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 25.12.2021 03:47 |
 * Created with IntelliJ IDEA
 */
public interface NumbersService {
    String next();

    String random();
}
