package ru.pcs.web.services;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

/**
 * Project: carNumberGenerator
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 25.12.2021 03:48 |
 * Created with IntelliJ IDEA
 */
@Component
public class NumbersServiceImpl implements NumbersService {
    /**
     * Максимальное количество уникальных номеров 12(букв)^3(шт) * 999(цифры) == 1728 * 999
     */
    private static final Integer MAX_COUNT_NUMBERS = 1726272;

    /**
     * Массив валидных букв для гос номеров
     */
    private static final String[] LETTERS = {"А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х"};

    /**
     * Константа для каждого номера
     */
    private static final String RUS = " 116 RUS";

    /**
     * Коллекция уникальных гос номеров
     */
    private final Set<String> numbersRepository;

    /**
     * Логгер
     */
    private final Logger log;

    /**
     * Гос номер
     */
    private final String[] carNumber;

    /**
     * Индекс букв для массива LETTERS
     */
    private final Integer[] letterIndex;

    /**
     * Число от 1 до 999 для гос номера
     */
    private Integer digitForCarNumber;

    public NumbersServiceImpl() {
        this.numbersRepository = ConcurrentHashMap.newKeySet();
        this.carNumber = new String[]{"А", "000", "А", "А", RUS};
        this.log = Logger.getLogger(NumbersServiceImpl.class.getName());
        this.letterIndex = new Integer[]{0, 0, 0};
        this.digitForCarNumber = 0;
    }

    /**
     * @return следующее уникальное число
     */
    @Override
    public String next() {
        if (numbersRepository.size() == MAX_COUNT_NUMBERS) {
            log.info("All car numbers for the region \"" + RUS + "\" are issued!");
            return "All car numbers for the region \"" + RUS + "\" are issued!";
        }
        while (true) {
            nextCarNumberEngine();
            if (isNumberUniqueAddInRepo("next()"))
                return String.join("", carNumber);
        }
    }

    /**
     * Движок метода next(), переходит на следующий уникальный гос номер.
     * Если текущий номер последний, например, "Х999ХХ 116 RUS", но все варианты номеров не использованы, то переключает
     * на самый первый гос номер;
     */
    private void nextCarNumberEngine() {
        if (carNumber[1].equals("999")) {
            carNumber[1] = "001";
            if (letterIndex[2] == 11) {
                letterIndex[2] = 0;
                carNumber[3] = LETTERS[letterIndex[2]];
                if (letterIndex[1] == 11) {
                    letterIndex[1] = 0;
                    carNumber[2] = LETTERS[letterIndex[1]];
                    if (letterIndex[0] == 11)
                        letterIndex[0] = 0;
                    else
                        letterIndex[0] += 1;
                    carNumber[0] = LETTERS[letterIndex[0]];
                } else {
                    letterIndex[1] += 1;
                    carNumber[2] = LETTERS[letterIndex[1]];
                }
            } else {
                letterIndex[2] += 1;
                carNumber[3] = LETTERS[letterIndex[2]];
            }
        } else {
            digitForCarNumber = Integer.parseInt(carNumber[1]) + 1;
            carNumber[1] = checkDischargeOfDigit(digitForCarNumber);
        }
    }

    /**
     * Метод генерирует рандомные буквы и цифры для гос номера. Для ускорения работы метода последние 5000 гос номеров
     * создаёт метод next()
     *
     * @return рандомный автомобильный номер
     */
    @Override
    public String random() {
        if (numbersRepository.size() > (MAX_COUNT_NUMBERS - 5000))
            return next();
        while (true) {
            for (int i = 0; i < letterIndex.length; i++)
                letterIndex[i] = ThreadLocalRandom.current().nextInt(12);
            carNumber[0] = LETTERS[letterIndex[0]];
            digitForCarNumber = ThreadLocalRandom.current().nextInt(998) + 1;
            carNumber[1] = checkDischargeOfDigit(digitForCarNumber);
            carNumber[2] = LETTERS[letterIndex[1]];
            carNumber[3] = LETTERS[letterIndex[2]];
            carNumber[4] = RUS;
            if (isNumberUniqueAddInRepo("random()"))
                return String.join("", carNumber);
        }
    }

    /**
     * Проверка разряда числа, добавление нулей, если число меньше 10 или 100
     *
     * @param digit рандомное число от 1 до 999
     * @return число в формате "001" "011"
     */
    private String checkDischargeOfDigit(Integer digit) {
        if (digit < 10)
            return "00" + digit;
        else if (digit < 100)
            return "0" + digit;
        return Integer.toString(digit);
    }

    /**
     * Проверка уникальности номера с ранее выданными номерами, если уникальный - добавление и запись в лог
     *
     * @param methodName имя метода для записи в лог
     * @return если номер уникален - true, иначе - false
     */
    private boolean isNumberUniqueAddInRepo(String methodName) {
        if (numbersRepository.add(Arrays.toString(carNumber))) {
            log.info("№ " + numbersRepository.size() + '\t' + "| " +
                    String.join("", carNumber) + " |" + '\t' + methodName);
            return true;
        }
        return false;
    }
}
