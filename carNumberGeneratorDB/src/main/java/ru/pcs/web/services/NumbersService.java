package ru.pcs.web.services;

import ru.pcs.web.models.Number;

import java.util.List;

/**
 * Project: carNumberGeneratorDB
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 25.12.2021 03:47 |
 * Created with IntelliJ IDEA
 */
public interface NumbersService {
    String next();

    String random();

    List<Number> getAllNumbers();

    void addNumber(Number form);
}
