package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Number;

import java.util.List;

/**
 * Project: carNumberGeneratorDB
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 13.01.2022 17:12 |
 * Created with IntelliJ IDEA
 */
public interface NumbersRepository extends JpaRepository<Number, Integer> {
    List<Number> findByNumber(String number);
}
