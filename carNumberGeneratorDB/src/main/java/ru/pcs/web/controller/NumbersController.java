package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.pcs.web.models.Number;
import ru.pcs.web.services.NumbersService;

/**
 * Project: carNumberGeneratorDB
 *
 * @author Nikita Lyonin
 * Email:  nekitlenin@gmail.com |
 * Create: 25.12.2021 03:39 |
 * Created with IntelliJ IDEA
 */
@RequiredArgsConstructor
@Controller
public class NumbersController {

    private final NumbersService numbersService;

    @GetMapping("number/random")
    public String getRandomNumber(Model model) {
        String randomNumber = numbersService.random();
        Number number = new Number();

        number.setNumber(randomNumber);
        numbersService.addNumber(number);
        model.addAttribute("randomNumber", randomNumber);
        model.addAttribute("numbers", numbersService.getAllNumbers());
        return "randomNumber";
    }

    @GetMapping("number/next")
    public String getNextNumber(Model model) {
        String nextNumber = numbersService.next();
        Number number = new Number();

        number.setNumber(nextNumber);
        numbersService.addNumber(number);
        model.addAttribute("nextNumber", nextNumber);
        model.addAttribute("numbers", numbersService.getAllNumbers());
        return "nextNumber";
    }
}
